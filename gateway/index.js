const httpProxy = require("express-http-proxy");
const express = require("express");
const app = express();
const logger = require("morgan");

app.use(logger("dev"));

const selectProxyHost = (req) => {
  if (req.path.startsWith("/api/species")) {
    return "http://localhost:8081/";
  } else if (req.path.startsWith("/api/planets")) {
    return "http://localhost:8082/";
  }
};

app.use((req, res, next) => {
  httpProxy(selectProxyHost(req))(req, res, next);
});

app.listen(10000, () => {
  console.log("API Gateway running!");
});
