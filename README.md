# Projeto
## _Programa para copiar a API Swapi.dev para os microservices implementados_


## Como rodar:
1. Tenha **openjdk-16**
1. Tenha **PostgresSQL** com um usuário **postgres** e senha **postgres** e um banco de dados **postgres** na porta **5432**
1. Tenha **PostgresSQL** com um usuário **postgres** e senha **postgres** e um banco de dados **postgres** na porta **5433**
1. Tenha **node** e **yarn**

1. Esteja na pasta ./species do projeto
1. Utilize mvn/mvnw/mvn.cmd package
1. Rode com java -jar ./target/species-1.0.jar

1. Esteja na pasta ./planets do projeto
1. Utilize mvn/mvnw/mvn.cmd package
1. Rode com java -jar ./target/planets-1.0.jar

1. Esteja na pasta ./gateway do projeto
1. Rode o **yarn install**
1. Rode o **yarn start**


1. Esteja na pasta ./integration do projeto
1. Utilize mvn/mvnw/mvn.cmd package
1. Rode com java -jar ./target/integration-1.0.jar

