# Species
## _Cópia do endpoint /api/species do swapi.dev_


## Como rodar:
1. Tenha **openjdk-16**
1. Tenha **PostgresSQL** com um usuário **postgres** e senha **postgres** e um banco de dados **postgres** na porta **5432**
1. Utilize mvn/mvnw/mvn.cmd package
1. Esteja na pasta ./species do projeto
1. Rode com java -jar ./target/species-1.0.jar
## Endpoints disponiveis:
 1. GET /api/species/{:id}
 1. POST /api/spepcies
Caso esteja rodando o projeto, estará disponível em: http://localhost:8081/swagger-ui.html
