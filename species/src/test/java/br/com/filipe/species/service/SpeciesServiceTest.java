package br.com.filipe.species.service;

import br.com.filipe.species.exception.SpecieAlreadyExistsException;
import br.com.filipe.species.exception.SpecieNotFoundException;
import br.com.filipe.species.model.Species;
import br.com.filipe.species.repository.SpeciesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;


class SpeciesServiceTest {

    Species speciesMock;
    Optional<Species> speciesOptional;

    SpeciesService speciesService;

    @Mock
    SpeciesRepository speciesRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        speciesService = new SpeciesService(speciesRepository);

        speciesMock = Species.builder()
                .id(10L)
                .name("Espécie")
                .classification("Classificação")
                .designation("Designação")
                .average_height("Média de Peso")
                .skin_colors("Cor da pele")
                .hair_colors("Cor do cabelo")
                .eye_colors("Cor dos olhos")
                .films(Set.of("Filme 01", "Filme 02"))
                .build();
    }

    /**
     * Caso teste de sucesso para achar a espécie no Repositório.
     */
    @Test
    void findByIdSuccess() {
        speciesOptional = Optional.of(speciesMock);
        when(speciesRepository.findById(anyLong())).thenReturn(speciesOptional);

        Species result = speciesService.findById(speciesMock.getId());

        Assertions.assertNotNull(result);

        Assertions.assertEquals(
                speciesMock,
                result
        );
    }

    /**
     * Caso teste de erro para achar a espécie no Repositório.
     */
    @Test
    void findByIdError() {
        when(speciesRepository.findById(anyLong())).thenReturn(Optional.empty());

        Exception exception = Assertions.assertThrows(SpecieNotFoundException.class, () -> {
            speciesService.findById(speciesMock.getId());
        });

        String expectedMessage = "Esta espécie não foi encontrada no banco.";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    /**
     * Caso teste de sucesso para criar uma espécie no repositório.
     */
    @Test
    void createVehicleSuccess() {
        speciesOptional = Optional.of(speciesMock);

        when(speciesRepository.save(any(Species.class))).thenReturn(speciesMock);

        Species result = speciesService.create(speciesMock);

        Assertions.assertNotNull(result);

        Assertions.assertEquals(
                speciesMock,
                result
        );
    }

    /**
     * Caso teste de erro para criar uma espécie no repositório.
     */
    @Test
    void createVehicleError() {
        speciesOptional = Optional.of(speciesMock);

        when(speciesRepository.existsById(anyLong())).thenReturn(true);

        Exception exception = Assertions.assertThrows(SpecieAlreadyExistsException.class, () -> {
            speciesService.create(speciesMock);
        });

        String expectedMessage = "Não foi possível salvar, pois já existe a espécie no banco.";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

}