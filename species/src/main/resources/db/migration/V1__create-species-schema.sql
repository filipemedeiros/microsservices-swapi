CREATE TABLE species
(
    id               BIGINT NOT NULL PRIMARY KEY,
    name             VARCHAR(255),
    classification   VARCHAR(255),
    designation      VARCHAR(255),
    average_height   VARCHAR(255),
    skin_colors      VARCHAR(255),
    hair_colors      VARCHAR(255),
    eye_colors       VARCHAR(255),
    average_lifespan VARCHAR(255),
    homeworld        VARCHAR(255),
    language         VARCHAR(255),
    created          VARCHAR(255),
    edited           VARCHAR(255),
    url              VARCHAR(255),
    CONSTRAINT id UNIQUE (id)
);

CREATE TABLE people
(
    species_id BIGINT NOT NULL REFERENCES species(id),
    people VARCHAR(255)
);

CREATE TABLE films
(
    species_id BIGINT NOT NULL REFERENCES species(id),
    films VARCHAR(255)
);