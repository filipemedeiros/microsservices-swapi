package br.com.filipe.species.service;

import br.com.filipe.species.model.Species;
import br.com.filipe.species.exception.SpecieAlreadyExistsException;
import br.com.filipe.species.exception.SpecieNotFoundException;
import br.com.filipe.species.repository.SpeciesRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class SpeciesService {

    private final SpeciesRepository speciesRepository;

    private boolean existsById(Long id) {
        log.debug("Verificando se a espécie de ID: {} existe no banco.", id);
        Boolean existe = speciesRepository.existsById(id);
        if (existe.equals(Boolean.TRUE)) {
            log.warn("A espécie de ID: {} já existe no banco.", id);
        }
        return existe;
    }

    public Species create(Species specieBody) {
        if (existsById(specieBody.getId())) {
            throw new SpecieAlreadyExistsException("Não foi possível salvar, pois já existe a espécie no banco.");
        } else {
            log.info("Criando a espécie de ID: {} no Banco.", specieBody.getId());
            Species specie = Species.builder()
                    .id(specieBody.getId())
                    .name(specieBody.getName())
                    .classification(specieBody.getClassification())
                    .designation(specieBody.getDesignation())
                    .average_height(specieBody.getAverage_height())
                    .skin_colors(specieBody.getSkin_colors())
                    .hair_colors(specieBody.getHair_colors())
                    .eye_colors(specieBody.getEye_colors())
                    .average_lifespan(specieBody.getAverage_lifespan())
                    .homeworld(specieBody.getHomeworld())
                    .language(specieBody.getLanguage())
                    .people(specieBody.getPeople())
                    .films(specieBody.getFilms())
                    .url(specieBody.getUrl())
                    .created(specieBody.getCreated())
                    .edited(specieBody.getEdited())
                    .build();

            return speciesRepository.save(specie);
        }
    }

    public Species findById(Long id) {
        log.debug("Buscando a espécie de ID: {} no banco.", id);
        Species specie = speciesRepository.findById(id)
                .orElseThrow(() -> {
                    log.error("Espécie com ID: {} não Encontrado.", id);
                    throw new SpecieNotFoundException("Esta espécie não foi encontrada no banco.");
                });
        log.info("A espécie de ID: {} foi encontrado no banco com sucesso!", id);
        return specie;
    }
}
