package br.com.filipe.species.controller;

import br.com.filipe.species.exception.SpecieAlreadyExistsException;
import br.com.filipe.species.exception.SpecieNotFoundException;
import br.com.filipe.species.model.Species;
import br.com.filipe.species.service.SpeciesService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class RestApiController {

    SpeciesService speciesService;

    @ApiOperation(value = "Retorna uma espécie especificada.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna uma espécie especificada."),
            @ApiResponse(code = 404, message = "Não foi encontrado a determinada espécie."),
    })
    @GetMapping("/species/{id}")
    public ResponseEntity<?> getSpecie(@PathVariable long id) {
        ResponseEntity<?> response;
        try {
            Species specie = speciesService.findById(id);
            response = new ResponseEntity<>(specie, HttpStatus.OK);
        } catch (SpecieNotFoundException e) {
            response = new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @ApiOperation(value = "Cadastra uma espécie especificada.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna a espécie especificada."),
            @ApiResponse(code = 408, message = "A espécie já existe."),
    })
    @PostMapping("/species")
    public ResponseEntity<?> postSpecies(@RequestBody Species speciesBody) {
        ResponseEntity<?> response;
        try {
            Species specie = speciesService.create(speciesBody);
            response = new ResponseEntity<>(specie, HttpStatus.OK);
        } catch (SpecieAlreadyExistsException e) {
            response = new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_ACCEPTABLE);
        }
        return response;
    }
}