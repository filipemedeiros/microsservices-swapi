package br.com.filipe.species.exception;

public class SpecieAlreadyExistsException extends RuntimeException {
    public SpecieAlreadyExistsException(String message) {
        super(message);
    }
}
