package br.com.filipe.species.repository;


import br.com.filipe.species.model.Species;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpeciesRepository extends JpaRepository<Species, Long> {}