package br.com.filipe.species.exception;

public class SpecieNotFoundException extends RuntimeException {
    public SpecieNotFoundException(String message) {
        super(message);
    }
}
