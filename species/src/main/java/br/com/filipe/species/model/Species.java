package br.com.filipe.species.model;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Set;


@Entity
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Species {

    @Id
    public Long id;
    public String name;
    public String classification;
    public String designation;
    public String average_height;
    public String skin_colors;
    public String hair_colors;
    public String eye_colors;
    public String average_lifespan;
    public String homeworld;
    public String language;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="people")
    public Set<String> people;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="films")
    public Set<String> films;

    public String created;
    public String edited;
    public String url;
}
