CREATE TABLE planets
(
    id              BIGINT NOT NULL PRIMARY KEY,
    name            VARCHAR(255),
    rotation_period VARCHAR(255),
    orbital_period  VARCHAR(255),
    diameter        VARCHAR(255),
    climate         VARCHAR(255),
    gravity         VARCHAR(255),
    terrain         VARCHAR(255),
    population      VARCHAR(255),
    surface_water   VARCHAR(255),
    created         VARCHAR(255),
    edited          VARCHAR(255),
    url             VARCHAR(255),

    CONSTRAINT id UNIQUE (id)
);

CREATE TABLE residents
(
    planets_id BIGINT NOT NULL REFERENCES planets (id),
    residents  VARCHAR(255)
);

CREATE TABLE films
(
    planets_id BIGINT NOT NULL REFERENCES planets (id),
    films      VARCHAR(255)
);