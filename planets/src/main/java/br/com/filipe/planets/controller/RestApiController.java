package br.com.filipe.planets.controller;

import br.com.filipe.planets.exception.PlanetsAlreadyExistsException;
import br.com.filipe.planets.exception.PlanetsNotFoundException;
import br.com.filipe.planets.model.Planets;
import br.com.filipe.planets.service.PlanetsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class RestApiController {

    PlanetsService planetsService;

    @ApiOperation(value = "Retorna um planeta especificado.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna um planeta especificado."),
            @ApiResponse(code = 404, message = "Não foi encontrado o determinado planeta."),
    })
    @GetMapping("/planets/{id}")
    public ResponseEntity<?> getPlanets(@PathVariable long id) {
        ResponseEntity<?> response;
        try {
            Planets specie = planetsService.findById(id);
            response = new ResponseEntity<>(specie, HttpStatus.OK);
        } catch (PlanetsNotFoundException e) {
            response = new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return response;
    }

    @ApiOperation(value = "Cadastra um planeta especificadp.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna o planeta especificado."),
            @ApiResponse(code = 408, message = "O planeta já existe."),
    })
    @PostMapping("/planets")
    public ResponseEntity<?> postPlanets(@RequestBody Planets planetsBody) {
        ResponseEntity<?> response;
        try {
            Planets planets = planetsService.create(planetsBody);
            response = new ResponseEntity<>(planets, HttpStatus.OK);
        } catch (PlanetsAlreadyExistsException e) {
            response = new ResponseEntity<>(e.getMessage(),HttpStatus.NOT_ACCEPTABLE);
        }
        return response;
    }
}