package br.com.filipe.planets.exception;

public class PlanetsNotFoundException extends RuntimeException {
    public PlanetsNotFoundException(String message) {
        super(message);
    }
}
