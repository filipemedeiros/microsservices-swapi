package br.com.filipe.planets.repository;


import br.com.filipe.planets.model.Planets;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanetsRepository extends JpaRepository<Planets, Long> {}