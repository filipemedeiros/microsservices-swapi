package br.com.filipe.planets.exception;

public class PlanetsAlreadyExistsException extends RuntimeException {
    public PlanetsAlreadyExistsException(String message) {
        super(message);
    }
}
