package br.com.filipe.planets.service;

import br.com.filipe.planets.model.Planets;
import br.com.filipe.planets.exception.PlanetsAlreadyExistsException;
import br.com.filipe.planets.exception.PlanetsNotFoundException;
import br.com.filipe.planets.repository.PlanetsRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class PlanetsService {

    private final PlanetsRepository planetsRepository;

    private boolean existsById(Long id) {
        log.debug("Verificando se o planeta de ID: {} existe no banco.", id);
        Boolean existe = planetsRepository.existsById(id);
        if (existe.equals(Boolean.TRUE)) {
            log.warn("O planeta de ID: {} já existe no banco.", id);
        }
        return existe;
    }

    public Planets create(Planets planetBody) {
        if (existsById(planetBody.getId())) {
            throw new PlanetsAlreadyExistsException("Não foi possível salvar, pois já existe o planeta no banco.");
        } else {
            log.info("Criando o planeta de ID: {} no Banco.", planetBody.getId());
            Planets planet = Planets.builder()
                    .id(planetBody.getId())
                    .name(planetBody.getName())
                    .rotation_period(planetBody.getRotation_period())
                    .orbital_period(planetBody.getOrbital_period())
                    .diameter(planetBody.getDiameter())
                    .gravity(planetBody.getGravity())
                    .terrain(planetBody.getTerrain())
                    .surface_water(planetBody.getSurface_water())
                    .population(planetBody.getPopulation())
                    .residents(planetBody.getResidents())
                    .films(planetBody.getFilms())
                    .climate(planetBody.getClimate())
                    .url(planetBody.getUrl())
                    .created(planetBody.getCreated())
                    .edited(planetBody.getEdited())
                    .build();

            return planetsRepository.save(planet);
        }
    }

    public Planets findById(Long id) {
        log.debug("Buscando o planeta de ID: {} no banco.", id);
        Planets planet = planetsRepository.findById(id)
                .orElseThrow(() -> {
                    log.error("Planeta com ID: {} não Encontrado.", id);
                    throw new PlanetsNotFoundException("Este planeta não foi encontrada no banco.");
                });
        log.info("O planeta de ID: {} foi encontrado no banco com sucesso!", id);
        return planet;
    }
}
