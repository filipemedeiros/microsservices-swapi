package br.com.filipe.planets.model;

import lombok.*;

import javax.persistence.*;
import java.util.Set;


@Entity
@Builder
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Planets {

    @Id
    public Long id;
    public String name;
    public String rotation_period;
    public String orbital_period;
    public String diameter;
    public String climate;
    public String gravity;
    public String terrain;
    public String surface_water;
    public String population;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="residents")
    public Set<String> residents;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name="films")
    public Set<String> films;
    public String created;
    public String edited;
    public String url;
}
