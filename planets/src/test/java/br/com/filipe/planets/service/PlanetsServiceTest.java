package br.com.filipe.planets.service;

import br.com.filipe.planets.exception.PlanetsAlreadyExistsException;
import br.com.filipe.planets.exception.PlanetsNotFoundException;
import br.com.filipe.planets.model.Planets;
import br.com.filipe.planets.repository.PlanetsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;
import java.util.Set;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;


class PlanetsServiceTest {

    Planets planetsMock;
    Optional<Planets> planetsOptional;

    PlanetsService planetsService;

    @Mock
    PlanetsRepository planetsRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        planetsService = new PlanetsService(planetsRepository);

        planetsMock = Planets.builder()
                .id(10L)
                .name("planeta")
                .rotation_period("10h")
                .orbital_period("100h")
                .diameter("500")
                .films(Set.of("Filme 01", "Filme 02"))
                .build();
    }

    /**
     * Caso teste de sucesso para achar a espécie no Repositório.
     */
    @Test
    void findByIdSuccess() {
        planetsOptional = Optional.of(planetsMock);
        when(planetsRepository.findById(anyLong())).thenReturn(planetsOptional);

        Planets result = planetsService.findById(planetsMock.getId());

        Assertions.assertNotNull(result);

        Assertions.assertEquals(
                planetsMock,
                result
        );
    }

    /**
     * Caso teste de erro para achar a espécie no Repositório.
     */
    @Test
    void findByIdError() {
        when(planetsRepository.findById(anyLong())).thenReturn(Optional.empty());

        Exception exception = Assertions.assertThrows(PlanetsNotFoundException.class, () -> {
            planetsService.findById(planetsMock.getId());
        });

        String expectedMessage = "Este planeta não foi encontrada no banco.";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    /**
     * Caso teste de sucesso para criar uma espécie no repositório.
     */
    @Test
    void createVehicleSuccess() {
        planetsOptional = Optional.of(planetsMock);

        when(planetsRepository.save(any(Planets.class))).thenReturn(planetsMock);

        Planets result = planetsService.create(planetsMock);

        Assertions.assertNotNull(result);

        Assertions.assertEquals(
                planetsMock,
                result
        );
    }

    /**
     * Caso teste de erro para criar uma espécie no repositório.
     */
    @Test
    void createVehicleError() {
        planetsOptional = Optional.of(planetsMock);

        when(planetsRepository.existsById(anyLong())).thenReturn(true);

        Exception exception = Assertions.assertThrows(PlanetsAlreadyExistsException.class, () -> {
            planetsService.create(planetsMock);
        });

        String expectedMessage = "Não foi possível salvar, pois já existe o planeta no banco.";
        String actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

}