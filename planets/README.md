# Planets
## _Cópia do endpoint /api/planets do swapi.dev_


## Como rodar:
1. Tenha **openjdk-16**
1. Tenha **PostgresSQL** com um usuário **postgres** e senha **postgres** e um banco de dados **postgres** na porta **5433**
1. Utilize mvn/mvnw/mvn.cmd package
1. Esteja na pasta ./planets do projeto
1. Rode com java -jar ./target/planets-1.0.jar
## Endpoints disponiveis:
 1. GET /api/planets/{:id}
 1. POST /api/planets
Caso esteja rodando o projeto, estará disponível em: http://localhost:8082/swagger-ui.html
