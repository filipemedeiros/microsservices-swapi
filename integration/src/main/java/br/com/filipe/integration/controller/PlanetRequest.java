package br.com.filipe.integration.controller;

import br.com.filipe.integration.model.Planets;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PlanetRequest {

    @POST("/api/planets")
    Call<Planets> post(@Body Planets planets);
}