package br.com.filipe.integration.controller;

import br.com.filipe.integration.model.Planets;
import br.com.filipe.integration.model.Species;
import com.google.gson.internal.LinkedTreeMap;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface SwapiRequest {

    @GET("/api/planets/{id}")
    Call<Planets> getPlanets(@Path("id") Long id);

    @GET("/api/species/")
    Call<LinkedTreeMap> getSpecies();

    @GET("/api/planets/")
    Call<LinkedTreeMap> getPlanets();

    @GET("/api/species/{id}")
    Call<Species> getSpecies(@Path("id") Long id);


}