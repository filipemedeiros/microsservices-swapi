package br.com.filipe.integration.sender;

import br.com.filipe.integration.model.Planets;
import br.com.filipe.integration.model.Species;
import br.com.filipe.integration.model.Type;
import br.com.filipe.integration.service.RequestService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@AllArgsConstructor
@Slf4j
public class Processor {

    private static final int DEZ_MINUTOS = 600000;
    RequestService requestService;

    @Async
    @Scheduled(fixedDelay = DEZ_MINUTOS)
    public void processPlanets() {
        log.info("Inicializando Processamento do ENDPOINT /planets");
        int size = requestService.getSize(Type.PLANETS);
        for (long id = 1; id <= size; id++) {
            Planets planet = requestService.getPlanets(id);
            if (Objects.nonNull(planet)) {
                Planets response = requestService.post(id, planet);
            }
        }
        log.info("Finalizado o Processamento do ENDPOINT /planets");

    }

    @Async
    @Scheduled(fixedDelay = DEZ_MINUTOS)
    public void processSpecies() {
        log.info("Inicializando Processamento do ENDPOINT /species");
        int swapiSize = requestService.getSize(Type.SPECIES);
        for (long id = 1; id <= swapiSize; id++) {
            Species specie = requestService.getSpecies(id);
            if (Objects.nonNull(specie)) {
                Species response = requestService.post(id, specie);
            }
        }
        log.info("Finalizado o Processamento do ENDPOINT /species");
    }

}