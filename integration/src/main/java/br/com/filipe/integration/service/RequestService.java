package br.com.filipe.integration.service;


import br.com.filipe.integration.controller.PlanetRequest;
import br.com.filipe.integration.controller.SpeciesRequest;
import br.com.filipe.integration.controller.SwapiRequest;
import br.com.filipe.integration.model.Planets;
import br.com.filipe.integration.model.Species;
import br.com.filipe.integration.model.Type;
import com.google.gson.internal.LinkedTreeMap;
import lombok.extern.log4j.Log4j2;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.Objects;

@Service
@Log4j2
public class RequestService {

    private static final String API_GATEWAY = "http://localhost:10000/api/";
    private static final String API_BASE_URL = "https://swapi.dev/api/";
    SwapiRequest swapiRequest;
    PlanetRequest planetRequest;
    SpeciesRequest speciesRequest;

    public RequestService() {
        swapiRequest = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(SwapiRequest.class);

        planetRequest = new Retrofit.Builder()
                .baseUrl(API_GATEWAY)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(PlanetRequest.class);

        speciesRequest = new Retrofit.Builder()
                .baseUrl(API_GATEWAY)
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(SpeciesRequest.class);
    }

    @Retryable(value = IOException.class)
    public Planets getPlanets(Long id) {
        log.debug("Enviando a Requisição: GET {}{}{}", API_BASE_URL, "planets/", id);
        Call<Planets> retrofitCall = swapiRequest.getPlanets(id);
        Planets response = null;
        try {
            response = retrofitCall.execute().body();
            if (Objects.nonNull(response)) {
                log.debug("Foi retornando o planeta: " + response.getName());
                log.debug("---------");
            }
        } catch (IOException e) {
            log.error("Requisição falhou no envio:  GET {}{}{}", API_BASE_URL, "planets/", id);
        }
        return response;
    }

    @Retryable(value = IOException.class, label = "Tentando")
    public Species getSpecies(Long id) {
        log.debug("Enviando a Requisição: GET {}{}{}", API_BASE_URL, "species/", id);
        Call<Species> retrofitCall = swapiRequest.getSpecies(id);
        Species response = null;
        try {
            response = retrofitCall.execute().body();
            if (Objects.nonNull(response)) {
                log.debug("Foi retornando a espécie: " + response.getName());
                log.debug("---------");
            }
        } catch (IOException e) {
            log.error("Requisição falhou no envio:  GET {}{}{}", API_BASE_URL, "species/", id);
        }
        return response;
    }


    @Retryable(value = IOException.class)
    public int getSize(Type type) {
        log.debug("Enviando a Requisição: GET {}{}", API_BASE_URL, type);

        Call<LinkedTreeMap> retrofitCall;
        if (Objects.equals(type, Type.PLANETS)) {
            retrofitCall = swapiRequest.getPlanets();
        } else {
            retrofitCall = swapiRequest.getSpecies();
        }
        int response = 0;

        try {
            if (Objects.nonNull(type)) {
                LinkedTreeMap body = retrofitCall.execute().body();
                response = ((Double) body.get("count")).intValue();
            }
        } catch (IOException e) {
            log.error("Requisição falhou em: " + API_BASE_URL + "planets/");
        }
        log.debug("Foi encontrado: {} itens", response);

        return response;
    }

    public Planets post(Long id, Planets planetBody) {
        log.debug("Enviando a Requisição: POST {}{}", API_GATEWAY, "planets/");

        Planets planet = Planets.builder()
                .id(id)
                .name(planetBody.getName())
                .rotation_period(planetBody.getRotation_period())
                .orbital_period(planetBody.getOrbital_period())
                .diameter(planetBody.getDiameter())
                .gravity(planetBody.getGravity())
                .terrain(planetBody.getTerrain())
                .surface_water(planetBody.getSurface_water())
                .population(planetBody.getPopulation())
                .residents(planetBody.getResidents())
                .films(planetBody.getFilms())
                .climate(planetBody.getClimate())
                .url(planetBody.getUrl())
                .created(planetBody.getCreated())
                .edited(planetBody.getEdited())
                .build();

        Planets response = null;
        try {
            Call<Planets> retrofitCall = planetRequest.post(planet);
            response = retrofitCall.execute().body();
            if (Objects.nonNull(response)) {
                log.debug("Foi enviado com sucesso o Planeta: {}", response.getName());
                log.debug("---------");
            }
        } catch (IOException e) {
            log.warn("Requisição falhou no envio:  POST {}{} com ID: {}", API_GATEWAY, "planets/", id);
        }
        return response;
    }

    public Species post(Long id, Species specieBody) {
        log.debug("Enviando a Requisição: POST {}{}", API_GATEWAY, "species/");

        Species specie = Species.builder()
                .id(id)
                .name(specieBody.getName())
                .classification(specieBody.getClassification())
                .designation(specieBody.getDesignation())
                .average_height(specieBody.getAverage_height())
                .skin_colors(specieBody.getSkin_colors())
                .hair_colors(specieBody.getHair_colors())
                .eye_colors(specieBody.getEye_colors())
                .average_lifespan(specieBody.getAverage_lifespan())
                .homeworld(specieBody.getHomeworld())
                .language(specieBody.getLanguage())
                .people(specieBody.getPeople())
                .films(specieBody.getFilms())
                .url(specieBody.getUrl())
                .created(specieBody.getCreated())
                .edited(specieBody.getEdited())
                .build();
        Species response = null;

        try {
            Call<Species> retrofitCall = speciesRequest.post(specie);
            response = retrofitCall.execute().body();
            if (Objects.nonNull(response)) {
                log.debug("Foi enviado com sucesso a espécie: {} ", response.getName());
                log.debug("---------");
            }
        } catch (IOException e) {
            log.error("Requisição falhou no envio:  POST {}{} com ID: {}", API_GATEWAY, "species/", id);
        }
        return response;
    }
}
