package br.com.filipe.integration.controller;

import br.com.filipe.integration.model.Species;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface SpeciesRequest {

    @POST("/api/species")
    Call<Species> post(@Body Species species);
}